function myIterators(start, finish) {
    let index = start
    let count = 0

    return {
        next() {
            let result;
            if (index < finish) {
                result = {value: index, done: false}
                index += 1
                count ++
                return result
            }
            return {
                value: count,
                done: true
            }
        }
    }
}

    const it = myIterators(0, 10)

const arr = [0, 3, 4, 6]

const iter = arr[Symbol.iterator]()
console.log(iter.next())
console.log(iter.next())
console.log(iter.next())
console.log(iter.next())
console.log(iter.next())

const map = new Map()
map.set('key1', 'value1')
map.set('key2', 'value2')
map.set('key3', 'value3')
const mapIterator = map[Symbol.iterator]()
console.log(mapIterator.next().value)
console.log(mapIterator.next().value)
console.log(mapIterator.next().value)


for (const [key, value] of map) {
    console.log((`${key} and ${value}`))
}

const mySet = new Set()
mySet.add('uno')
mySet.add('dos')

const setIterator = mySet[Symbol.iterator]()
console.log('')
console.log(setIterator.next())
console.log(setIterator.next())

for (let val, result; (result = it.next()) && !result.done; ) {
val = result.value
    console.log(val)
}

