function * timeStampGenerator() {
   var ts = Date.now()
    console.log('original ts: ', ts)
    yield ts
    console.log('boo')
    var additionalTime = yield
    console.log('additional time' + ' ' + additionalTime)
    if (additionalTime) {
        ts = ts + additionalTime
    }
    console.log('Updated ts)' + ' ' + ts)
}

const itTime = timeStampGenerator()
 const originalTimeStamp = itTime.next()
 console.log(originalTimeStamp)
 itTime.next()
// itTime.next(1000 * 60)

