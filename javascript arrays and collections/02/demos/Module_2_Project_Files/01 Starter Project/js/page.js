// Charts
let ctx = document.getElementById('monthlySales').getContext('2d')
let pieCtx = document.getElementById('deptSales').getContext('2d')
let yearlyLabel = document.getElementById('yearlyTotal')

let monthlySales = Array.of(12000, 9000, 3000,400)
let monthlyLabels = Array.of('Oct', 'Nov', 'Dec')

let deptSales = Array.of(12,9,3)
let deptLabels = Array.of('Hiking', 'Running', 'Hunting')

let yearlyTotal = 0

function addYearlyTotal(x) {
yearlyTotal = x + yearlyTotal
}
monthlySales.forEach(addYearlyTotal)

function resetToZero() {
    monthlySales.fill(0)
    monthlySalesChart.update()
}

let octNums = Array.of(12000,9000,3000)
let novNums = Array.of(11000,6000,2000)
let decNums = Array.of(8000,10000,5000)

// let total = Array.of(addYearlyTotal(...octNums), addYearlyTotal(... novNums), addYearlyTotal(...decNums))

 yearlyLabel.innerHTML = "$" + yearlyTotal

function findOver1000() {
let firstThousand = monthlySales.findIndex(element => element > 1000)
    alert(firstThousand)
}

//Bar
var monthlySalesChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: monthlyLabels,
        datasets: [{
            label: '# of Sales',
            data: monthlySales,
            backgroundColor: [
                'rgba(238, 184, 104, 1)',
                'rgba(75, 166, 223, 1)',
                'rgba(239, 118, 122, 1)',
            ],
            borderWidth: 0
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

// Pie
var deptSalesChart = new Chart(pieCtx, {
    type: 'pie',
    data: {
        labels: deptLabels,
        datasets: [{
            label: '# of Sales',
            data: deptSales,
            backgroundColor: [
                'rgba(238, 184, 104, 1)',
                'rgba(75, 166, 223, 1)',
                'rgba(239, 118, 122, 1)',
            ],
            borderWidth: 0
        }]
    },
    options: {

    }
})
